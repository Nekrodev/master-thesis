import pandas as pd
import matplotlib.pyplot as plt
import applicationSettings
import numpy as np
from classes.point import Point
from classes.connection import Connection
from classes.line import Line
from classes.pantograph import PantographState
import matplotlib.animation as animation
from celluloid import Camera

def parseData(inputFilePath, separator):
    file = pd.read_csv(inputFilePath, sep=separator)
    x = file['x']
    y = file['y']
    index = file['index']
    i = file['i']
    j = file['j']
    fixed = filter( lambda x: not np.isnan(x), list(file['fixed']))

    points = [] 
    connections = []
    for k in range(len(x)):
        points.append(Point(x[k], y[k], int(index[k])))
    for k in range(len(i)):
        connections.append(Connection(i[k], j[k]))
    return points, connections, list(fixed)

points, connections, fixed = parseData(applicationSettings.inputFilePath, ';')
state = PantographState(points, connections, fixed)
points, connections, fixed = parseData(applicationSettings.endFilePath, ';')
anotherState = PantographState(points, connections, fixed)
anotherState.setUpReference(state)

fig = plt.figure()
camera = Camera(fig)

for i in range(0, 62):
    anotherState.fitMinima(i/6)
    anotherState.plot()
    plt.legend([f'iteration {i}'])
    camera.snap()
animation = camera.animate()
animation.save('deformation.gif', writer = 'pillow')
plt.show()