import pandas as pd
import matplotlib.pyplot as plt
import applicationSettings
import numpy as np
import math
from classes.point import Point
from classes.connection import Connection
from classes.line import Line
from classes.pantograph import PantographState
from parsers import parseData, plotForceDisp
import matplotlib.animation as animation
from celluloid import Camera
import time


def buildModel(incrementedDisplacement_x: float, incrementedDisplacement_y: float, numberOfIteration: int):
    print('Parsing started...')
    points, connections, fixed = parseData(applicationSettings.inputFilePath, ';')
    state = PantographState(points, connections, fixed)
    points, connections, fixed = parseData(applicationSettings.inputFilePath, ';')
    anotherState = PantographState(points, connections, fixed)
    anotherState.setUpReference(state)

    displacement = 0
    dispList = []
    energyList = []
    force = []
    print('Parsing finished, start main loop...')
    for i in range(numberOfIteration):
        start_time = time.time()
        anotherState.imposeDisplaycement(10, incrementedDisplacement_x, incrementedDisplacement_y)
        anotherState.imposeDisplaycement(11, incrementedDisplacement_x, incrementedDisplacement_y)
        anotherState.fitMinima()
        print("Iterations takes %s seconds ---" % (time.time() - start_time))
        displacement += math.sqrt(incrementedDisplacement_x**2 + incrementedDisplacement_y**2)
        dispList.append(displacement)
        energy = anotherState.getEnergy()
        energyList.append(energy)
        print(f'iteration energy: {energy}')

    for i in range(len(energyList) - 1):
        force.append(energyList[i + 1] - energyList[i])
    forceDispList = list.copy(dispList)
    del forceDispList[0]

    return forceDispList, force
