import numpy as np
import applicationSettings
import math


def cos(vector_1, vector_2):
    return np.dot(vector_1, vector_2) / (np.linalg.norm(vector_2)*np.linalg.norm(vector_1) + 0.0000001)


def angle(vector_1, vector_2) -> float:
    return np.arccos(cos(vector_1, vector_2))


def calculateBendingEnergy(vectors) -> float:
    pointVectors = vectors.copy()
    energy = 0
    while len(pointVectors) > 0:
        vector_1 = pointVectors.pop()
        vector_2 = max(pointVectors, key=lambda x: abs(
            np.arccos(cos(x, vector_1))))
        pointVectors.remove(vector_2)
        energy += (cos(vector_1, vector_2) + 1)
    return 1/2*applicationSettings.b*energy


def calculateStrainEnergy(referencePoints, currentPoints) -> float:
    energy = 0
    for i in range(len(referencePoints)):
        energy += (referencePoints[i] - currentPoints[i])**2

    return 1/2 * applicationSettings.a * energy


def closure(n, i) -> int:
    return i % n


def calculateTorsionEnergy(vectors) -> float:
    energy = 0
    for i in range(len(vectors)):
        energy += (angle(vectors[i],
                         vectors[closure(len(vectors), i + 1)]) - math.pi/2)**2/(31.4**2)
    return 1/2*applicationSettings.s*energy
