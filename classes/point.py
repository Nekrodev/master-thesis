import math
from helpers import calculateTorsionEnergy, calculateBendingEnergy, calculateStrainEnergy
import applicationSettings
import numpy as np

class Point:

    def __init__(self, x, y, index, isMain = True):
        self.x = x
        self.y = y
        self.algIndex = 1000
        self.isFixed = False
        self.index = index
        self.neighbors = []
        self.isMain = isMain
        self.reference = self

    def __repr__(self):
        return str(self.x) + " " +str(self.y)
    def __str__(self):
        return str(self.x) + " " +str(self.y)
    
    def setIsFixed(self, isFixed: bool):
        self.isFixed = isFixed

    def setReference(self, reference):
        self.reference = reference

    def getReference(self):
        return self.reference

    def addNeighbor(self, other):
        self.neighbors.append(other)
    
    def getNeighbors(self):
        return self.neighbors

    def getNeighborVectors(self):
        vectors = []
        for point in self.neighbors:
            vectors.append(self.getVector(point))
        return vectors

    def calculateTorsionEnergy(self) -> float:
        if len(self.neighbors) > 1 and self.isMain:
            return calculateTorsionEnergy(self.getNeighborVectors())
        return 0

    def calculateBendingEnergy(self) -> float:
        if len(self.neighbors) == 4:
            return calculateBendingEnergy(self.getNeighborVectors())
        if len(self.neighbors) == 2 and not self.isMain:
            return calculateBendingEnergy(self.getNeighborVectors())
        return 0

    def calculateStrainEnergy(self) -> float:
        energy = 0
        currentVectors = self.getNeighborVectors()
        referenceVectors = self.reference.getNeighborVectors()
        for i in range(len(currentVectors)):
            energy += (np.linalg.norm(currentVectors[i]) - np.linalg.norm(referenceVectors[i]))**2
        return 1/2* applicationSettings.a*energy

    def getEnergy(self, d_x = 0, d_y = 0) -> float:
        x = self.x
        y = self.y
        self.x += d_x
        self.y += d_y
        energy = self.calculateTorsionEnergy() + self.calculateBendingEnergy() + self.calculateStrainEnergy()
        self.x = x
        self.y = y
        return math.sqrt(abs(energy))

    def divide(self, other, fraction: float):
        _x = (self.x + fraction*other.x)/(1 + fraction)
        _y = (self.y + fraction*other.y)/(1 + fraction)
        _index = self.index + fraction + other.index*100
        return Point(_x, _y, _index, False)

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def __sub__(self, other):
        return math.sqrt((self.x - other.x)**2 + (self.y - other.y)**2)

    def getVector(self, end):
        return [end.x - self.x, end.y - self.y]
    def getSecondDerivative(self):
        delta = applicationSettings.delta
        energy = self.getEnergy()
        plus = (self.getEnergy(d_x = delta) - energy)/ delta
        minus = (energy - self.getEnergy(d_x = - delta))/ delta
        x = (plus - minus)/delta
        plus = (self.getEnergy(d_y = delta) - energy)/ delta
        minus = (energy - self.getEnergy(d_y = - delta))/ delta
        y = (plus - minus)/delta
        return [x, y]
    
    def fitMinima(self, alpha = 0.05, loopLimit = 0):
        delta = applicationSettings.delta
        prevEnergy = self.getEnergy()
        x = self.x
        y = self.y
        grad_x = (self.getEnergy(d_x = delta) - self.getEnergy( d_x = -delta))/ (2*delta)
        grad_y = (self.getEnergy(d_y = delta) - self.getEnergy( d_y = -delta))/ (2*delta)
        self.x -= alpha*grad_x
        self.y -= alpha*grad_y

        if prevEnergy < self.getEnergy() and loopLimit < 5:
            self.x = x
            self.y = y
            return self.fitMinima(alpha/2, loopLimit + 1)

        return grad_x**2 + grad_y**2



