import matplotlib.pyplot as plt
import applicationSettings
import numpy as np
import math
import copy
from classes.point import Point
from classes.connection import Connection
from classes.line import Line
from helpers import calculateBendingEnergy, cos, calculateTorsionEnergy, calculateStrainEnergy


class PantographState:
    def __init__(self, points, connections, fixedPointsIndexes):
        self.points = points
        self.connection = connections
        fixed = []
        for elem in fixedPointsIndexes:
            fixed.append(int(elem))
        self.fixedPointsIndexes = fixed
        lines = []
        for connection in connections:
            startPoint = self.findPointByIndex(connection.i)
            endPoint = self.findPointByIndex(connection.j)
            lines.append(Line(startPoint, endPoint))
        self.lines = lines
        self.isMarked = False
        innerPoints = []
        for line in lines:
            innerPoints.extend(line.innerPoints)
        self.innerPoints = innerPoints

    def copy(self):
        return copy.deepcopy(self)

    def plot(self, fig):
        ax = fig.add_subplot(111, aspect=1.0)
        for line in self.lines:
            line.plot(ax)

    def imposeDisplaycement(self, index: int, d_x: float, d_y: float):
        point = self.findPointByIndex(index)
        point.x += d_x
        point.y += d_y

    def getPoints(self) -> list:
        points = []
        points.extend(self.points)
        points.extend(self.innerPoints)
        return points

    def setUpReference(self, reference):
        for i in range(len(self.points)):
            self.points[i].setReference(reference.points[i])
        for i in range(len(self.innerPoints)):
            self.innerPoints[i].setReference(reference.innerPoints[i])

    def findPointByIndex(self, index: int) -> Point:
        return next(x for x in self.points if x.index == index)

    def E_a(self) -> float:
        energy = 0
        for point in self.getPoints():
            energy += point.calculateStrainEnergy()
        return energy

    def E_b(self) -> float:
        energy = 0
        for point in self.getPoints():
            energy += point.calculateBendingEnergy()
        return energy

    def E_s(self) -> float:
        energy = 0
        for point in self.getPoints():
            energy += point.calculateTorsionEnergy()
        return energy

    def getEnergy(self) -> float:
        return self.E_a() + self.E_b() + self.E_s()

    def detectDisplacedPoints(self):
        displacedPoints = []
        for index in self.fixedPointsIndexes:
            point = self.findPointByIndex(index)
            if (point.calculateStrainEnergy() > applicationSettings.accuracy):
                displacedPoints.append(point)

        return displacedPoints

    def initFitting(self):
        if (self.isMarked):
            return
        self.isMarked = True
        self.imposedPoints = self.detectDisplacedPoints()
        neighbors = []
        for displacedPoint in self.imposedPoints:
            displacedPoint.algIndex = 0
            neighbors.extend(displacedPoint.getNeighbors())
        self.markCycle(neighbors, 0, [])

    def fitMinima(self):
        self.initFitting()
        self.minCycle()

    def getForce(self):
        points = self.getPoints()
        force = []
        for point in points:
            force.extend(point.getSecondDerivative())
        print(force)
        return np.linalg.norm(force)

    def markCycle(self, neighbors, algIndex, fitted=[]):
        size = len(fitted)
        newNeighbors = []
        for point in neighbors:
            if (point.index not in fitted):
                point.algIndex = min((algIndex + 1, point.algIndex))
                fitted.append(point.index)
                newNeighbors.extend(point.getNeighbors())
        if (size == len(fitted)):
            return
        if (len(newNeighbors) == 4):
            for neighbor in newNeighbors:
                if (neighbor.index not in fitted):
                    self.markCycle([neighbor], algIndex + 1, fitted.copy())
        else:
            self.markCycle(newNeighbors, algIndex + 1, fitted)

    def minCycle(self):
        points = self.getPoints()
        prevEnergy = self.getEnergy()
        energy = prevEnergy + 2
        counter = 0
        alpha = 0
        coeff = 1
        loopLimit = 0
        while energy > 1 and counter < applicationSettings.iterationLimit:
            alpha = coeff/energy
            prevEnergy = energy
            energy = 0
            counter += 1
            points.sort(key=lambda x: x.algIndex)
            for point in points:
                if (point.index not in self.fixedPointsIndexes):
                    energy += point.fitMinima(alpha, loopLimit)
            if (prevEnergy < energy):
                coeff = coeff / 2
                alpha = coeff/energy
            points.sort(key=lambda x: 1000-x.algIndex)
            for point in points:
                if (point.index not in self.fixedPointsIndexes):
                    energy += math.sqrt(point.fitMinima(alpha, loopLimit))
        print(f'Iteration finishes by {counter}, with grad {energy}')
