class Connection:
    def __str__(self):
        return str([self.i, self.j])

    def __init__(self, i, j):
        if (i < j):
            self.i = i
            self.j = j
        else:
            self.i = j
            self.j = i
