import matplotlib.pyplot as plt
import applicationSettings
from classes.point import Point
from classes.connection import Connection
from helpers import calculateBendingEnergy


class Line:
    def getPoints(self):
        points = self.innerPoints.copy()
        points.append(self.endPoint)
        points.insert(0, self.startPoint)
        return points

    def __init__(self, startPoint, endPoint):
        between = applicationSettings.between
        self.startPoint = startPoint
        self.endPoint = endPoint
        innerPoints = []
        for i in range(1, between + 1):
            innerPoints.append(startPoint.divide(
                endPoint, i/(between + 1 - i)))
        self.innerPoints = innerPoints
        
        points = self.getPoints()
        for i in range(len(points) - 1):
            points[i].addNeighbor(points[i+1])
            points[i+1].addNeighbor(points[i])

    def plot(self, ax): #Todo reImplement with respect to inner points
        x = []
        y = []
        for point in self.getPoints():
            x.append(point.x)
            y.append(point.y)

        ax.plot(x, y, color='red', linewidth=3,
                     marker='.', markerfacecolor='grey', markersize=9)
        for point in (self.startPoint, self.endPoint):
            ax.plot(point.x, point.y, marker='.', markerfacecolor='blue', markersize=13)

    def E_b(self):
        energy = 0
        points = self.innerPoints.copy()
        points.append(self.endPoint)
        points.insert(0, self.startPoint)
        for i in range(1, len(points) - 1):
            vector_1 = points[i].getVector(points[i-1])
            vector_2 = points[i].getVector(points[i+1])
            energy += calculateBendingEnergy(vector_1, vector_2)
        return energy

    def getVector(self):
        return self.startPoint.getVector(self.endPoint)