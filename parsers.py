import pandas as pd
import matplotlib.pyplot as plt
import applicationSettings
import numpy as np
from classes.point import Point
from classes.connection import Connection
from classes.line import Line
from classes.pantograph import PantographState
import matplotlib.animation as animation
from celluloid import Camera
import time

def parseData(inputFilePath, separator):
    file = pd.read_csv(inputFilePath, sep=separator)
    x = list(filter(lambda x: not np.isnan(x), list(file['x'])))
    y = list(filter(lambda x: not np.isnan(x), list(file['y'])))
    index = list(filter(lambda x: not np.isnan(x), list(file['index'])))
    i = list(filter(lambda x: not np.isnan(x), list(file['i'])))
    j = list(filter(lambda x: not np.isnan(x), list(file['j'])))
    fixed = list(filter(lambda x: not np.isnan(x), list(file['fixed'])))

    points = []
    connections = []
    for k in range(len(x)):
        points.append(Point(x[k], y[k], int(index[k])))
    for k in range(len(i)):
        connections.append(Connection(i[k], j[k]))
    return points, connections, list(fixed)

def plotForceDisp(dispList, force, name = 'force.png'):
    plt.plot(dispList, force)
    plt.savefig('plots/' + name)
    plt.show()

def buildWithPlot():
    print('Parsing started...')
    points, connections, fixed = parseData(applicationSettings.inputFilePath, ';')
    state = PantographState(points, connections, fixed)
    points, connections, fixed = parseData(applicationSettings.inputFilePath, ';')
    anotherState = PantographState(points, connections, fixed)
    anotherState.setUpReference(state)
    fig = plt.figure()
    camera = Camera(fig)
    incrementedDisplacement = 0.05
    displacement = 0
    dispList = []
    energyList = []
    force = []
    print('Parsing finished, start main loop...')
    for i in range(20):
        start_time = time.time()
        anotherState.imposeDisplaycement(10, 0, incrementedDisplacement)
        anotherState.imposeDisplaycement(11, 0, incrementedDisplacement)
        anotherState.fitMinima()
        print("Iterations takes %s seconds ---" % (time.time() - start_time))
        anotherState.plot(fig)

        displacement += incrementedDisplacement
        dispList.append(displacement)
        energy = anotherState.getEnergy()
        energyList.append(energy)

        plt.legend([f'iteration {i}'])
        camera.snap()
        print(f'iteration energy: {energy}')

    print('Creating animations...')
    animation = camera.animate()
    animation.save('plots/deformation.gif', writer = 'pillow')
    plt.show()
    for i in range(len(energyList) - 1):
        force.append(energyList[i+1] - energyList[i])
    plotForceDisp(dispList, energyList, 'energy')
    del dispList[0]
    plotForceDisp(dispList, force)