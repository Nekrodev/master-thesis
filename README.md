## Pantograph modeling


## Parser
1. Parser will take input file that is defined in applicationSettings

## Classes
1. `Point` contains coordinate of the point, point's index and some additional methods for simplification of calculations.
2. `Line` contains startPoint and endPoint, innerPoints,represents each line segment in pantograph
3. `Connection` contains connections between points, each connection contains 2 indexes of the point.
4. `PantographState` contains all method for calculations of energy, consist from Points, InnerPoints ( not present in base geometry,the amount of inner points can be set in applicationSettings.py file)


## Helpers
File contains helpful mathematical functions.

## Applications Settings
Can be changed in applicationSettings.py
