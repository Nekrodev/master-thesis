import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import applicationSettings
from matplotlib.animation import FuncAnimation
import time

def split(input):
    result = []
    line = []
    for elem in input:
        if (np.isnan(elem)):
            result.append(line)
            line = []
        else:
            line.append(elem)
    return result

# plt.plot(x, y, color='red', linewidth = 3,
#          marker='>', markerfacecolor='blue', markersize=12)
# plt.ylim(np.nanmin(y) - 1, np.nanmax(y) +1)
# plt.xlim(np.nanmin(x) - 1, np.nanmax(x) +1)
# plt.show()
fig = plt.figure()
ax = plt.axes(xlim=(-1, 5), ylim=(-1, 5))
line, = ax.plot([], [], lw=3)

def init():
    line.set_data([], [])
    return line,

def animate(i):
    time.sleep(0.4)
    file = pd.read_csv('train.csv', sep=';')
    x = file['x']
    y = file['y']
    x = split(x)
    y = split(y)
    position = i % len(x)
    line.set_data(x[position], y[position])
    return line,

anim = FuncAnimation(fig, animate, init_func=init,
                               frames=200, interval=20, blit=True)

plt.show()
# anim.save('sine_wave.gif', writer='imagemagick')