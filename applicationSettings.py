#input files:
inputFilePath = 'csv/input.csv'
endFilePath = 'csv/end.csv'
sampleDataFilePath = 'csv/sample_tension.csv'

# number of point between two initial connected with line segment:
between = 3

#model parameters:
b = 2
s = 2.5
a = 6.5

descendCoefficient = 0.01

iterationLimit = 200

accuracy = 0.000001

delta = 0.001