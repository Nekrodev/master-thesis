import pandas as pd
import applicationSettings
import matplotlib.pyplot as plt


def createMeanValueList(basic: list, meanSize: int) -> list:
    steps = int(len(basic) / meanSize)
    result = []

    for i in range(steps):
        summ = 0
        for j in range(meanSize):
            summ += basic[i + j] / meanSize
        result.append(summ)
    return result


def applyManyMean(basic: list, meanSize: int, times: int):
    result = basic
    for i in range(times):
        result = createMeanValueList(result, int(meanSize / (i + 1)))
    return result


def convertToList(dataFrame: object) -> list:
    result = []
    for element in dataFrame:
        result.append(element)
    return result


def getMeanValuedData(meanSize: int, times: int):
    data = pd.read_csv(applicationSettings.sampleDataFilePath, sep=',').drop(
        columns="Temperature").drop(columns="Points")
    displacement = applyManyMean(convertToList(data["Disp"]), meanSize, times)
    load = applyManyMean(convertToList(data["Load 2"]), meanSize, times)

    if len(displacement) != len(load):
        raise Exception("Length must be the same")
    return displacement, load


def getPreparedInputData():
    return getMeanValuedData(12, 2)
